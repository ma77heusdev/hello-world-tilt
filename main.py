import logging

from fastapi import FastAPI

app = FastAPI()

@app.on_event("startup")
async def startup():
    logging.info("Starting application...")
    logging.info("Ready-to-use application.")

@app.get("/say_hello")
async def root() -> dict:
    logging.info("send log!")
    return {"message": "Hello!!"}

@app.get("/err")
async def erro() -> dict:
    logging.info("error")
    l = []
    # return l["err"]

logging.basicConfig(level=logging.INFO)
