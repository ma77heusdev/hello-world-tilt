# hello-world-tilt

### Prerequisites

install [tilt](https://docs.tilt.dev/install.html)
install [docker](https://docs.docker.com/engine/install/)

change start.sh with your kubeconfig

Configure basic tiltfile [docs](https://docs.tilt.dev/api.html):

- default_registry("registry"): Sets the default container registry that Tilt will use to push container images.

- docker_build("registry", "path_to_build"): Build the image and send it to the registry (for docker-compose use docker_compose('./docker-compose.yml'))

- allow_k8s_contexts("context"): Connect to the specified cluster

- k8s_yaml("file.yml"): Defines which yml will be sent to the cluster

### Running the app

./start.sh

### Local Test

For local testing without the availability of a cluster use [kind](https://kind.sigs.k8s.io/)
